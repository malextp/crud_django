from django.db import models

# Create your models here.
class Course(models.Model):
    name = models.CharField(max_length=140)
    start_date = models.CharField(max_length=140)
    end_date = models.CharField(max_length=140)
    picture = models.CharField(max_length=140)
    ola = models.CharField(max_length=140)