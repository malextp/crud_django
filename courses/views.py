from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Course

# Create your views here.

class CourseList(ListView):
    model = Course
    template_name = 'courses/course_list.html'

class CourseDetail(DetailView):
    model = Course
    template_name = 'courses/course_detail.html'

class CourseCreation(CreateView):
    model = Course
    success_url = reverse_lazy('courses:list')
    fields = ['name', 'start_date', 'end_date', 'picture', 'ola']

class CourseUpdate(UpdateView):
    model = Course
    success_url = reverse_lazy('courses:list')
    fields = ['name', 'start_date', 'end_date', 'picture', 'ola']

class CourseDelete(DeleteView):
    model = Course
    template_name = 'courses/courseconfirmdelete.html'
    success_url = reverse_lazy('courses:list')