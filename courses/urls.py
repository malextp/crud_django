from django.conf.urls import url
from django.urls import path
from .views import CourseList, CourseDetail, CourseCreation, CourseUpdate, CourseDelete

app_name = 'courses'
urlpatterns = [
    path('', CourseList.as_view(), name='list'),
    path('<pk>', CourseDetail.as_view(), name='detail'),
    path('nuevo/', CourseCreation.as_view(), name='new'),
    path('editar/<pk>', CourseUpdate.as_view(), name='edit'),
    path('borrar/<pk>', CourseDelete.as_view(), name='delete'),

]